"""Закон Амдала иллюстрирует ограничение роста производительности
 вычислительной системы с увеличением количества вычислителей.

 Number of workers: 1.
Sub took: 8.7997 seconds.
Took: 8.8625 seconds.
____________________
Number of workers: 2.
Sub took: 4.4217 seconds.
Took: 4.4564 seconds.
____________________
Number of workers: 3.
Sub took: 3.0224 seconds.
Took: 3.0603 seconds.
____________________
Number of workers: 4.
Sub took: 2.3203 seconds.
Took: 2.3620 seconds.
____________________
Number of workers: 5.
Sub took: 1.8998 seconds.
Took: 1.9508 seconds.
____________________
Number of workers: 6.
Sub took: 1.6751 seconds.
Took: 1.7305 seconds.
____________________
Number of workers: 7.
Sub took: 1.6967 seconds.
Took: 1.7508 seconds.
____________________
Number of workers: 8.
Sub took: 1.7413 seconds.
Took: 1.8118 seconds.
____________________
Number of workers: 9.
Sub took: 1.9472 seconds.
Took: 2.0244 seconds.
____________________
Number of workers: 10.
Sub took: 1.7736 seconds.
Took: 1.8627 seconds.
____________________
Number of workers: 11.
Sub took: 1.7565 seconds.
Took: 1.8836 seconds.
____________________
Number of workers: 12.
Sub took: 1.8810 seconds.
Took: 1.9795 seconds.
____________________"""
from math import sqrt

import concurrent.futures
import multiprocessing

from timeit import default_timer as timer


def is_prime(x):
    if x < 2:
        return False

    if x == 2:
        return x

    if x % 2 == 0:
        return False

    limit = int(sqrt(x)) + 1
    for i in range(3, limit, 2):
        if x % i == 0:
            return False

    return x


def concurrent_solve(n_workers):
    print('Number of workers: %i.' % n_workers)

    start = timer()
    result = []
    input_data = [i for i in range(10 ** 13, 10 ** 13 + 1000)]

    with concurrent.futures.ProcessPoolExecutor(max_workers=n_workers) as executor:

        futures = [executor.submit(is_prime, i) for i in input_data]
        completed_futures = concurrent.futures.as_completed(futures)

        sub_start = timer()

        for i, future in enumerate(completed_futures):
            if future.result():
                result.append(future.result())

        sub_duration = timer() - sub_start

    duration = timer() - start
    print('Sub took: %.4f seconds.' % sub_duration)
    print('Took: %.4f seconds.' % duration)

def run():

    for n_workers in range(1, multiprocessing.cpu_count() + 1):
        concurrent_solve(n_workers)
        print('_' * 20)


if __name__ == '__main__':
    run()