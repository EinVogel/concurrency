"""Сравнение времени выполнения последовательной и совместной программы с использованием библиотеки threading

Затраченное время последовательной обработки - 0.6462211608886719
Затраченное время совместной обработки - 0.9209821224212646"""
# single_threaded.py
import time

count = 5000000

def countdown():
    global count
    while count > 0:
        count -= 1

start = time.time()
countdown()
end = time.time()

print('Затраченное время последовательной обработки -', end - start)


# multi_threaded.py
import time
from threading import Thread

count = 5000000

def countdown():
    global count
    while count > 0:
        count -= 1

t1 = Thread(target=countdown)
t2 = Thread(target=countdown)

start = time.time()
t1.start()
t2.start()
t1.join()
t2.join()
end = time.time()

print('Затраченное время совместной обработки -', end - start)

