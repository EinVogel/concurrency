"""работа с локами, чтобы избежать состояния гонки"""
import concurrent.futures
import threading
from timeit import default_timer as timer
iterations = 20
lock = threading.Lock()

def work_func(result):
    result = result * result
    return result


start = timer()
result = 3
for i in range(iterations):
    result = work_func(result)

print('Sequential result:', str(result)[:10])
print('Sequential took: %.2f seconds.' % (timer() - start))


def concurrent_f(x):
    global result
    with lock:
        result = work_func(result)

start = timer()
result = 3

with concurrent.futures.ThreadPoolExecutor(max_workers=20) as executor:
    executor.map(concurrent_f, range(iterations))

print('Concurrent result:', str(result)[:10])
print('Concurrent took: %.2f seconds.' % (timer() - start))